module three_integer_input_m
    use veggies, only: input_t

    implicit none
    private
    public :: three_integer_input_t

    type, extends(input_t) :: three_integer_input_t
        private
        integer :: first_, second_, third_
    contains
        procedure :: first, second, third
    end type

    interface three_integer_input_t
        module procedure constructor
    end interface
contains
    pure function constructor(first, second, third) result(three_integer_input)
        integer, intent(in) :: first, second, third
        type(three_integer_input_t) :: three_integer_input

        three_integer_input%first_ = first
        three_integer_input%second_ = second
        three_integer_input%third_ = third
    end function

    pure function first(self)
        class(three_integer_input_t), intent(in) :: self
        integer :: first

        first = self%first_
    end function

    pure function second(self)
        class(three_integer_input_t), intent(in) :: self
        integer :: second

        second = self%second_
    end function

    pure function third(self)
        class(three_integer_input_t), intent(in) :: self
        integer :: third

        third = self%third_
    end function
end module
