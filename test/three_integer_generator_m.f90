module three_integer_generator_m
    use three_integer_input_m, only: three_integer_input_t
    use veggies, only: &
            generated_t, &
            generator_t, &
            input_t, &
            shrink_result_t, &
            get_random_integer, &
            shrunk_value, &
            simplest_value

    implicit none
    private
    public :: THREE_INTEGER_GENERATOR

    type, extends(generator_t) :: three_integer_generator_t
    contains
      procedure :: generate
      procedure, nopass :: shrink
    end type

    type(three_integer_generator_t), parameter :: THREE_INTEGER_GENERATOR = &
            three_integer_generator_t()
contains
    function generate(self) result(generated_value)
        class(three_integer_generator_t), intent(in) :: self
        type(generated_t) :: generated_value

        generated_value = generated_t(three_integer_input_t( &
                get_random_integer(), get_random_integer(), get_random_integer()))
    end function

    function shrink(input) result(shrunk)
        class(input_t), intent(in) :: input
        type(shrink_result_t) :: shrunk

        select type (input)
        type is (three_integer_input_t)
            associate(a => input%first(), b => input%second(), c => input%third())
                if (all([a, b, c] == 0)) then
                    shrunk = simplest_value(three_integer_input_t( &
                            0, 0, 0))
                else
                    shrunk = shrunk_value(three_integer_input_t( &
                            a/2, b/2, c/2))
                end if
            end associate
        end select
    end function
end module
