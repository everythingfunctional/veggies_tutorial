module add_test
    use iso_varying_string, only: operator(//)
    use strff, only: to_string
    use three_integer_generator_m, only: THREE_INTEGER_GENERATOR
    use three_integer_input_m, only: three_integer_input_t
    use veggies, only: &
            input_t, result_t, test_item_t, assert_equals, describe, fail, it

    implicit none
    private
    public :: test_add
contains
    function test_add() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "addition", &
                [ it("is associative", THREE_INTEGER_GENERATOR, check_associativity) &
                ])
    end function

    function check_associativity(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (three_integer_input_t)
            associate(a => input%first(), b => input%second(), c => input%third())
                result_ = assert_equals( &
                        a + (b + c), &
                        (a + b) + c, &
                        to_string(a) &
                        // " + " // to_string(b) &
                        // " + " // to_string(c))
            end associate
        class default
            result_ = fail("expected to get a three_integer_input_t")
        end select
    end function
end module
