module is_leap_year_test
    use is_leap_year_m, only: is_leap_year
    use strff, only: to_string
    use veggies, only: &
            example_t, &
            input_t, &
            integer_input_t, &
            result_t, &
            test_item_t, &
            assert_not, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_is_leap_year
contains
    function test_is_leap_year() result(tests)
        type(test_item_t) :: tests

        tests = describe(&
                "is_leap_year", &
                [ it( &
                        "returns false for years that are not divisible by 4", &
                        [ example_t(integer_input_t(2002)) &
                        , example_t(integer_input_t(2003)) &
                        ], &
                        check_not_divisible_by_4) &
                ])
    end function

    function check_not_divisible_by_4(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (integer_input_t)
            result_ = assert_not(is_leap_year(input%input()), to_string(input%input()))
        class default
            result_ = fail("Didn't get integer_input_t")
        end select
    end function
end module
