module stack_m
    implicit none
    private
    public :: stack_t

    type :: stack_t
        private
        integer, allocatable :: items(:)
    contains
        private
        procedure, public :: empty
        procedure, public :: top
        procedure, public :: pop
        procedure, public :: push
        procedure, public :: depth
    end type

    interface stack_t
        module procedure constructor
    end interface
contains
    pure function constructor() result(empty_stack)
        type(stack_t) :: empty_stack

        allocate(empty_stack%items(0))
    end function

    pure function empty(self)
        class(stack_t), intent(in) :: self
        logical :: empty

        empty = self%depth() == 0
    end function

    pure function top(self)
        class(stack_t), intent(in) :: self
        integer :: top

        if (self%empty()) then
            error stop "Asked for top of an empty stack."
        else
            top = self%items(1)
        end if
    end function

    pure function pop(self) result(popped)
        class(stack_t), intent(in) :: self
        type(stack_t) :: popped

        if (self%empty()) then
            error stop "Attempted to pop an empty stack."
        else
            if (self%depth() > 1) then
                allocate(popped%items, source = self%items(2:))
            else
                allocate(popped%items(0))
            end if
        end if
    end function

    pure function push(self, top) result(pushed)
        class(stack_t), intent(in) :: self
        integer, intent(in) :: top
        type(stack_t) :: pushed

        if (self%empty()) then
            allocate(pushed%items, source = [top])
        else
            allocate(pushed%items, source = [top, self%items])
        end if
    end function

    pure function depth(self)
        class(stack_t), intent(in) :: self
        integer :: depth

        if (allocated(self%items)) then
            depth = size(self%items)
        else
            depth = 0
        end if
    end function
end module
